import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
@Component({
  selector: 'app-rform',
  templateUrl: './rform.component.html',
  styleUrls: ['./rform.component.scss']
})
export class RformComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  saveEmployee(empForm: NgForm):void {
    console.log(empForm.value);
  }

}
