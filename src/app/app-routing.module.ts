import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RformComponent } from './rform/rform.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';


const routes: Routes = [
  {path: 'navbar' , component: NavBarComponent},
  {path:'rigestery', component:RformComponent},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent =[NavBarComponent,RformComponent]

